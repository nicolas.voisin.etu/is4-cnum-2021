# Calcul naïf d'une spline cubique naturelle
# On se donne n+1=3 points (x_i,y_i)
# On montre qu'il est possible de construire n=2 cubiques s_0 et s_1 dont les
#     graphes passent par les trois points et définissent une courbe globalement C2
# La méthode consiste à résoudre un système de 4*n équations linéaires à 4*n inconnues
# Une variante de l'interpolation de Hermite est mise en œuvre

import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

Tx = np.array ([1,3,4], dtype=np.float64)
Ty = np.array ([3,2,5], dtype=np.float64)

n = 2
A = np.zeros ([4*n,4*n], dtype=np.float64)
b = np.zeros (4*n, dtype=np.float64)

# le graphe de s_0 passe par (x_i,y_i) pour i = 0,1
A[0:2,0:4] = np.array ([[x**3, x**2, x, 1] for x in Tx[0:2]])
b[0:2] = Ty[0:2]

# le graphe de s_1 passe par (x_i,y_i) pour i = 1,2
A[2:4,4:8] = np.array ([[x**3, x**2, x, 1] for x in Tx[1:3]])
b[2:4] = Ty[1:3]

# Variante d'interpolation de Hermite
# s'_0(x) = s'_1(x) au nœud intérieur (en x = x_1)
A[4,0:8] = np.array ([[3*x**2, 2*x, 1, 0, -3*x**2, -2*x, -1, 0] for x in [Tx[1]]])
b[4] = 0

# s''_0(x) = s''_1(x) au nœud intérieur (en x = x_1)
A[5,0:8] = np.array ([[6*x, 2, 0, 0, -6*x, -2, 0, 0] for x in [Tx[1]]])
b[5] = 0

# Les conditions de spline naturelle :
# s''_0(x) = 0 au premier nœud et
A[6,0:4] = np.array ([[6*x, 2, 0, 0] for x in [Tx[0]]])
b[6] = 0

# s''_1(x) = 0 au dernier nœud
A[7,4:8] = np.array ([[6*x, 2, 0, 0] for x in [Tx[n]]])
b[7] = 0

a = nla.solve (A, b)

def s (x) :
    if x < Tx[1] :
        return ((a[0]*x + a[1])*x + a[2])*x + a[3]
    else :
        return ((a[4]*x + a[5])*x + a[6])*x + a[7]

plt.scatter (Tx, Ty)
h = .5

xplot = np.linspace (Tx[0]-.1*h, Tx[n]+.1*h, 100)
yplot = [s(x) for x in xplot]
plt.plot (xplot,yplot)
plt.show ()


