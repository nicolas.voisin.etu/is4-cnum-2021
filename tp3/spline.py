import numpy as np
import csv
import scipy.linalg as lng

# Constructuion d'un dictionnaire clé (age -float)
#                                 valeur (densits des patients d'age a)




res = {}  # initialisation dictionnaire

with open ('spnbms.csv') as f:
    reader = csv.DictReader(f, delimiter = ',')
    for row in reader :
        if row['ethnic'] in ['Hispanic'] and row['sex'] in ['mal'] :
            age = np.float64(row['age'])
            densite = np.float64(row['spnbmd'])
            if age in res :
                res[age].append(densite)
            else :
                res[age] = [densite]

#Constructuion des tableaux
x = np.array(sorted(res))
y = np.array([np.average(res[age])]) for age in  x], dtype = p.float64)

n = x.shape[0] - 1

def h(i,x):
    return x[i+1]-x[i]

T=np.zeros((n-1,n-1))

for i in range (1,n-2):
    T[i,i] = 2*(h(0,x)+h(2,x))
    T[i-1,i] = h(i,x)
    T[i+1,i] = h(i+1,x)

T[0,0] = 2*(h(0,x) + h(2,x))
T[1,0] = h(1,x)
T[n-2,n-2] = 2*(h(n-2,x)+h(n-1,x))
T[n-3,n-2] = h(n-2,x)
T=(1/3)*T

Q = np.zeros((n+1,n-1))

def g(i,x):
    return 1/(h(i,x))
for i in range (0,n-1):
    Q[i,i] = g(i,x)
    Q[i+1,i] = -g(i,x)-g(i+1,x)
    Q[i+2,i] = g(i+1,x)

def C(p):
    c = lng.solve(np.dot(np.transpose(Q),Q) + p*T, p*np.dot(np.transpose(Q),y))
    c[0] = 0
    c[len(c)-1] = 0
    return c

p = 1

c = C(p)

a = y - (1/p)*np.dot(Q,c)

d = []
d = np.array(d)

for i in range(len(c) - 2):
    np.append(d,(c[i+1] - c[i]) / (3*h(i,x)), axis=0)

b = []
b = np.array(b)
