#!/usr/bin/python3

import numpy as np
import csv

# Les données sont extraites de 'spnbmd.csv'
# 1. Construction de result = un dictionnaire contenant des valeurs
#       a: [d1, d2, ..., dn] (liste non vide)
#    où la clé a est un âge (flottant) et d1, d2, ..., dn sont les densités
#    des patients d'âge a. 

result = { }
with open ('spnbmd.csv') as f:
    reader = csv.DictReader(f, delimiter=',')
    for row in reader:
        if row['ethnic'] in ['Hispanic'] :
            if row['sex'] in ['mal']     :
                age = np.float64(row['age'])
                densite = np.float64(row['spnbmd'])
                if age in result :
                    result[age].append (densite)
                else :
                    result[age] = [ densite ]

# 2. Construction des tableaux x (trié par ordre croissant) et y
#   x[i] = un âge
#   y[i] = la moyenne des densités pour l'âge x[i]

x = np.array(sorted(result))
y = np.array([np.average(result[age]) for age in x], dtype=np.float64)

# les points sont indicés de 0 à n (inclus) comme dans le support de cours
n = x.shape[0] - 1
#1
p = 0

def estPaire (n) :
    rep = False 
    if (n % 2) == 0 :
        rep = True 
    return rep

#2
def h(X) :
    res = 0
    for i in range(0,len(X)-1) : 
        res += X[i+1] - X[i]
    return res 
#3 ???
n = 10
T = np.zeros(n-1,n-1)
cpt = 0
for i in range(n-1) :
    if estPaire(cpt) == True : 
        T[i,i] = 2 * (h(i) + h(i+1))
        T[i,i+1] = h(i+1)

    else :
        T[i,i-1] = h()
        
        


#4 

x = np.linalg.solve(T,Q)










        
    