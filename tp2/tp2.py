
#TP2
import numpy as np
import matplotlib.pyplot as plt 


#1) Intégration 

#Question 1
#fonction calculant l'erreur absolue 
def relerr (I,Ibis) :
    return (abs(I-Ibis))/abs(I)

#Question 2
#Fonction calculant le nombre de bits 
def nbbits(I, Ibis) :
    rlr = relerr(I, Ibis)
    return -np.log2(rlr)

#Question 3
I = 1024
Ibis = 1023

print("erreur relative :",relerr(I, Ibis))
print("\nnombre de bits :",nbbits(I, Ibis))


#Premier exemple 
#Question 4
def p1 (x) :
    t1 = 1/(np.log(2+x))
    t2 = np.sin(np.sqrt(x))
    return np.exp( t1 + t2 + 1)

def f1 (x) :
    t1 = np.cos(np.sqrt(x))/(2*np.sqrt(x))
    t2 = 1/( (np.log(2+x)**2)*(2+x)) 
    t3 = 1/(np.log(2+x)) + np.sin(np.sqrt(x)) + 1
    return ( ( t1 - t2) * np.exp(t3) )

#Question 5
def f2 (x) :
    t = (2*x**2) - 2*x + 1
    return 2/t

def p2 (x) :
    t = np.arctan(2*x - 1)
    return 2*t

#Methode des trapezes et de simpson 

"""
---------
Paramétres :
f : une fonction 
a & b : nombres flottant borne de l'intervalle d'intégration
N : le nombre de pas 
---------
Variable locales :
y0 & yn : premier et dernier terme cad f(a) et f(b)
h : le pas 
sum : variable temporaire 

Pour calculer le résultat on calcul premier terme divisé par 2 
ainsi que le dernier terme. 
Et on somme tout ce qu'il y a entre a et b par pas de h. 
En multipliant biensur le tout par h
"""
def Trapezes (f,a,b,N) : 
    h = (b-a)/N
    y0 = f(a)
    yn = f(b)
    sum = 0
    for i in np.arange(a,b,h):
        sum += f(i)
        
        
    return h * ( (y0/2) + sum + (yn/2) )
        
Trapezes(p2,0,1,1000)

#Fonction qui renvoie si un nombre est paire 
#Nous sera utile pour la formule de Simpson
def estPaire (n) :
    rep = False 
    if (n % 2) == 0 :
        rep = True 
    return rep
    
"""
---------
Paramétres :
f : une fonction 
a & b : nombres flottant borne de l'intervalle d'intégration
N : le nombre de pas 
---------
Variable locales :
y0 & yn : premier et dernier terme cad f(a) et f(b)
h : le pas 
sum : variable temporaire 
t : compteur pour determiner si on utilise la méthode paire ou impaire 

pour le calcul c'est simple: 
on calcul tout d'abord le premier terme et dernier terme
et une fois sur deux on additione soit par : 
4y(i) ou 2y(i)
en multipliant bien le tout par h/3 

"""
    
def Simpson (f,a,b,N) :
    h = (b-a)/N
    y0 = f(a)
    yn = f(b)
    t = 0
    sum = 0
    for i in np.arange(a,b,h) :
        if estPaire(t) == True :
            sum += 4*f(i)
        else :
            sum += 2*f(i)
        t+=1
    return (h/3)*(y0 + sum + yn) 


p = p1(10) - p1(1)
print("p = ",p)

T = Trapezes(f1,1,10,10000)
print("T = ",T)

S = Simpson(f1,1,10,10000)
print("S = ",S)
#nous voyons donc que nos fonctions sont correctes 


#Question 9
#Etude expérimentale de l'ordre des méthodes 

#N = 2**k avec k = 2,3......16
#B nbre bit 

#pour f1 

def analyseFonction (f,p,a,b) :
    T = 0
    #Trapeze
    for i in range (2,17) :
        T = Trapezes(f,a,b,2**i)
        P = p(a) - p(b)
        B = nbbits(T,P)
        plt.scatter(i, B, c = 'red')
    
    #Simpson
    for i in range (2,17) :
        S = Simpson(f,a,b,2**i)
        P = p(a) - p(b)
        B = nbbits(S,P)
        plt.scatter(i,B,c = 'blue')
    
    plt.show()

Trapezes(f1,p1,1,10)
