#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 19 21:49:02 2021

@author: nvoisin
"""
import numpy as np 
from scipy.interpolate import BarycentricInterpolator 
import matplotlib as plt 


"""
Fonction prenant en paramètre 2 tableaux de dimensions 
n+1 Tx (abscisees) et Ty (ordonnées)
un flottant nommée x 
cette fonction applique l'algorithme de Neville 

On construit le tableau des différences divisées de Newton
On utilise un schéma de Horner pour calculer la valeur du polynome d'interpolatio
'
"""

def Newton (Tx,Ty,x) :
    n = Tx.shape[0] - 1
    C = np.empty([n+1,n+1], dtype=np.float64)
    
    for i in range(0,n+1):
        C[i,0] = Ty[i]
    
    for j in range (1,n+1) :
        for i in range (0,n-j+1) :
            C[i,j] = (C[i+j,j-1] - C[i,j-1]) / (Tx[i+j] - Tx[i])
            
    t = x.shape[0]
    P = np.empty(t,dtype = np.float64)
    
    for k in range(0,t):
        p = C[0,n]
        for i in range(n-1, -1, -1):
            p = p * (x[k] - Tx[n-i]) + C[n-i,i]
        P[k] = p
        
    return p
    




Tx = np.array([1,2,3,5])

Ty = np.array([1,4,2,5])

X = np.linspace(0,5,50)
Y1 = Newton(Tx,Ty,X)
f = BarycentricInterpolator(Tx,Ty)
Y2 = np.array([f(k)] for k in X)

plt.scatter(Tx,Ty,color='red',label='Points interpolation')
 
plt.plot(X,Y1,color = 'orange', label = "Fonction interpolation Newton")

plt.plot(X,Y2,color = 'blue', label = "Fonction interpolation scipy")

plt.legend()
plt.show()




