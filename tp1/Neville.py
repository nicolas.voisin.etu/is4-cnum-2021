#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 26 14:21:15 2021

@author: nvoisin
"""
#Importation de numpy 
import numpy as np
import matplotlib.pyplot as plt 
from scipy import interpolate 



# On crée les deux tableaux de l'énoncé 
Tx = [1,2,3,5]
Ty = [1,4,2,5]
x = np.linspace(0,6,50)

"""
Fonction prenant en paramètre 2 tableaux de dimensions 
n+1 Tx (abscisees) et Ty (ordonnées)
un flottant nommée x 
cette fonction applique l'algorithme de Neville 
en retournant un polynôme d'interpolation
"""
def Neville (Tx,Ty,x) :
    n = len(Tx)
    p = n*[0]
    
    for j in range(n):
        for i in range(n-j):
            if j == 0 :
                 p[i] = Ty[i]
            else : 
                 p[i] = ((Tx[i]-x)*p[i+1]+(x-Tx[i+j])*p[i]) / (Tx[i]-Tx[i+j])

    return p


plt.scatter(Tx,Ty, color = 'blue', label = 'point_base')

xplot = x
yplot = np.array([Neville(Tx,Ty,x) for x in xplot])
plt.plot(xplot,yplot,color = 'red',label = 'graphe Neville')


P = interpolate.lagrange(Tx,Ty)
plt.plot(Tx,P,label = 'interpolation scipy')
plt.legend()
plt.show()





































